## About CREWS Webextension

This webextension was created with two goals from the
[CREWS](https://gitlab.torproject.org/groups/tpo/-/milestones/21) research
project:

1. Support _active resistance_
1. Support _collective resistance_

_Active_ resistance within the context of this research is the act of injecting
insecure HTTP requests between the web browser and the destination server
before the web browser automatically upgrades the security of the connection
and enforces TLS. This active resistance occurs in two scenarios:

1. The browser previously received a HSTS header from the web server, and
   therefore knows it should enforce TLS on any connection with that server
1. The browser is configured with a mode that attempts a TLS connection before
   (or instead of) it sending an insecure HTTP request (such as Firefox's
   [HTTPS-Only
   mode](https://blog.mozilla.org/security/2020/11/17/firefox-83-introduces-https-only-mode/))

An active resistance probe attempts to hide the fact that the browser
automatically upgrades the connection.

_Collective_ resistance within this context is a three-step process:

1. Participants collect [HSTS
   headers](https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers/Strict-Transport-Security)
   received from servers
1. Each participants' collection is used to create a set of known web sites
   enforcing TLS via HSTS
1. Browsers retrieve the set of known sites enforcing HSTS, and then adjust
   their behavior accordingly

## Bundling XPI (Firefox Webextension):
 Simply run `make`.

```
$ make
```

This will create `crews.xpi` and that can be temporarily installed into a
Firefox instance on `about:debugging`.

## Remaining work

- This webextension does not support Active Resistance due to insufficient
  control provided by the webextension API. This functionality is implemented
  [directly in the
  browser](https://gitlab.torproject.org/sysrqb/tor-browser/-/blob/bug_40296_04/CREWS_README.md).

- This webextension supports [RAPPOR](https://research.google/pubs/pub42852/)
  as a privacy-preserving foundation for collecting HSTS headers, but the
  actual collection functionality is not implemented.
