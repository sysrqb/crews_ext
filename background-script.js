class Request {
    constructor(details) {
        this.parsedUrl = splitURL(details.url);
        this.cookieStoreId = details.cookieStoreId;
        this.frameId = details.frameId;
        this.incognito = details.incognito;
        this.method = details.method;
        this.originUrl = details.originUrl;
        this.proxyInfo = details.proxyInfo;
        this.requestId = details.requestId;
        this.details = details;
        this.isActiveResistance = isActiveResistanceRequest(details);
        this.proxiedActiveResistance = false;
    }
}

let requests = {}

/**
 * Split a url string into its parts via an anchor tag
 */
function splitURL(url) {
    // Let the browser do the work
    let l = document.createElement("a");
    l.href = url;
    // see .protocol, .hostname, and .pathname of returned object
    return l
}

function hostAndPathMatch(url1, url2) {
    return url1.hostname == url2.hostname && url1.pathname == url1.pathname;
}

function isActiveResistanceRequest(details) {
    let detailOriginUrl = details.originUrl;
    if (detailOriginUrl !== undefined) {
        let originUrl = splitURL(detailOriginUrl);
        let pathToScript = browser.runtime.getURL("background-script.js");
        let pathUrl = splitURL(pathToScript);
        if (originUrl.protocol === pathUrl.protocol &&
            originUrl.hostname === pathUrl.hostname) {
            return true;
        }
    }
    return false;
}

async function onRequest_proxyActiveResistanceRequest(details) {
    console.log(`onRequest_proxyActiveResistanceRequest: ${details.requestId}`);
    let parsedUrl = splitURL(details.url);
    if (parsedUrl.protocol !== "https:") {
        console.log(`Got ${parsedUrl.protocol}?`);
        return;
    }
    for (requestId in requests) {
        let request = requests[requestId];
        if (request.redirectUrl === details.url) {

            // Cancel the Active Resistance connection if it was redirected
            // to https:, too. Don't initiate another Active Resistance request.
            // if (request.isActiveResistance) {
            //     console.log(`Canceling redirected Active Resistance request.`);
            //     delete requests[requestId];
            //     return { cancel: true };
            // }

            // If we arrived here, then the Active Resistance fetch was
            // redirected from http: to https:. Now we try using a
            // HTTP Connect instead of a GET request.
            // Initiate active resistance as HTTP Connect proxy request.
            if (request.isActiveResistance) {
                console.log("Using HTTP Connect as Active Resistance.");
                request.proxiedActiveResistance = true;
                let proxyInfo = {
                    type: "http",
                    host: details.parsedUrl.hostname,
                    port: 80,
                    proxyDNS: false
                };

                return proxyInfo;
            }
        }
    }

    console.log("Request not Active Resistance, not proxying.");
}

/**
 * Begin tracking request if it is for a http resource
 */
async function onBeforeRequest_beginTrackingHTTPRequest(details) {
    console.log(`onBeforeRequest_beginTrackingHTTPRequest: ${details.requestId}`);
    console.log(`Tracking request for ${details.url}.`);
    requests[details.requestId] = new Request(details);
}

/**
 * Deploy active resistance if this request is the result of HSTS
 */
async function onBeforeRequest_deployActiveResistance(details) {
    console.log(`onBeforeRequest_deployActiveResistance: ${details.requestId}`);
    for (requestId in requests) {
        let request = requests[requestId];
        if (request.redirectUrl === details.url) {
            if (request.isActiveResistance) {
                console.log("Not deploying Active Resistance for Active Resistance");
                return { cancel: false };
            }

            let origUrl = request.parsedUrl;
            let reqUrl = `http://${origUrl.hostname}:80/`;
            let fetchSettings = {
                method: "GET",
                credentials: "omit",
                cache: "no-store",
                redirect: "error",
                referrerPolicy: "no-referrer"
            };

            //let xmlHttp = new XMLHttpRequest();
            //xmlHttp.onreadystatechange = function() {
            //    if (xmlHttp.readyState == 4 && xmlHttp.status == 200) {
            //        return { cancel: false };
            //    }
            //}
            //xmlHttp.responseType = 'document';
            //xmlHttp.open("GET", reqUrl, true);
            //xmlHttp.send();

            //let ws = new WebSocket(reqUrl);
            console.log("Deploying Active Resistance");
            //return new Promise(xmlHttp.onreadystatechange);
            await fetch(reqUrl, fetchSettings)
                .then((val) => {
                    console.log("Active Resistance fetch succeeded."),
                    console.log(val)
                }, (err) => {
                    console.log("Active Resistance fetch failed."),
                    console.log(err)
                });

                delete requests[requestId];
                delete requests[details.requestId];
                return { cancel: false };
        }
    }
    console.log(`Not tracking request for ${details.url}`);
    return { cancel: false };
}

/**
 * If this is an HSTS redirect, then continue tracking it
 */
async function onBeforeRedirect_followRedirect(details) {
    console.log(`onBeforeRedirect_followRedirect: ${details.requestId}`);
    if (details.requestId in requests) {
        let origRequest = requests[details.requestId];
        let origUrl = origRequest.parsedUrl
        let redirectUrl = splitURL(details.redirectUrl);

        // If the redirect request is identical, except the protocol
        // changed from http: to https:.
        if (redirectUrl.protocol == "https:" &&
            hostAndPathMatch(redirectUrl, origUrl)) {
            requests[details.requestId].redirectUrl = details.redirectUrl;
            console.log(`Tracking redirect: ${details.statusCode}`);
        } else {
            console.log(`Deleting request, redirect is not HSTS: ${details.statusLine}`);
            delete requests[details.requestId];
        }
        return;
    }
    console.log(`Redirect not tracked: ${details.url}, ${details.statusLine}.`);
}

/**
 * End tracking request if it is for a http resource. The request was not
 * automatically redirected to https: by HSTS.
 */
async function onBeforeSendHeaders_endTrackingHTTPRequest(details) {
    console.log(`onBeforeSendHeaders_endTrackingHTTPRequest: ${details.requestId}`);
    delete requests[details.requestId];
}

function addEventListeners() {
    browser.proxy.onRequest.addListener(
        onRequest_proxyActiveResistanceRequest,
        {urls: ["https://*/*"]}, []
    );
    browser.webRequest.onBeforeRequest.addListener(
        onBeforeRequest_beginTrackingHTTPRequest,
        {urls: ["http://*/*"]}, []
    );
    browser.webRequest.onBeforeRequest.addListener(
        onBeforeRequest_deployActiveResistance,
        {urls: ["https://*/*"]}, ["blocking"]
    );
    browser.webRequest.onBeforeRedirect.addListener(
        onBeforeRedirect_followRedirect,
        {urls: ["http://*/*"]}, []
    );
    browser.webRequest.onBeforeSendHeaders.addListener(
        onBeforeSendHeaders_endTrackingHTTPRequest,
        {urls: ["http://*/*"]}, []
    );
}

addEventListeners();
