// This is an implementation of Rapper:
// https://arxiv.org/abs/1407.6981
// https://research.google/pubs/pub42852/
// https://github.com/google/rappor/blob/master/client/python/rappor.py

class RapporEncoder {
    constructor({
        numBloomBits = 16,
        numHashes = 2,
        numCohorts = 64,
        probP = 0.50,
        probQ = 0.75,
        probF = 0.50,
        cohort = 10,
        secret = "thisisas43cr3t",
        testing = false
    }) {
        // Length of bloom filter in bits
        self.numBloomBits = numBloomBits;
        // Number of bloom filter hash functions
        self.numHashes = numHashes;
        // Cohort size
        self.numCohorts = numCohorts;
        // Probability of 0 (B' = 0)
        self.probP = probP;
        // Probability of 1 (B' = 1)
        self.probQ = probQ;
        // Probability of randomization
        self.probF = probF;
        // User cohort
        self.cohort = cohort;
        // HMAC secret
        self.secret = secret;
        self.testing = testing;
    }

    async hmac(secret, message) {
        let cryptoKey = await window.crypto.subtle.importKey(
            "raw",
            secret,
            {
                name: "HMAC",
                hash: "SHA-256"
            }, 
            false, 
            ["sign"]
        );

        return await window.crypto.subtle.sign(
              "HMAC",
              cryptoKey,
              message
        );
    }

    //   mac = HMAC(secret, message)
    //   foreach bit in numBloomBits:
    //     byte = digest[bit]
    //     u_bit = byte & 0x01
    //     uniform |= (u_bit << i)
    //
    //     rand128 = byte >> 1
    //     noise_bit = (rand128 < threshold128)
    //     f_mask |= (noise_bit << i)
    //

    // Get Perminent Random Response
    // Input:
    //   inputValue: A four-octet big-endian bloom filter
    // Return:
    //   Object: uniformBitArray is a numBloomBits bit little-endian array
    //           biasBitArray is a numBloomBits bit little-endian array
    async getPermRandResp(inputValue) {
        const encoder = new TextEncoder();
        let encodedSecret = encoder.encode(self.secret);
        let hashedValue = await this.hmac(encodedSecret, inputValue);
        let hashedValueArr = new Uint8Array(hashedValue);

        // Set threshold at some fraction of 128
        let threshold = self.probF * 128;
        let uniformBitArray = new Uint8Array(self.numBloomBits);
        let biasBitArray = new Uint8Array(self.numBloomBits);
        let hexdigest = "";
        for (let i = 0; i < self.numBloomBits; i++) {
            let elm = hashedValueArr[i];
            hexdigest += Number(elm).toString(16);

            // Obtain 1 bit of entropy from least significant bit
            let entropy1Bit = elm & 0x01;
            // Obtain 7 bit of entropy from the remaining 7 significant bits of the octet
            let entropy7Bits = elm >> 1;

            // Set 1 bit of entropy in array
            uniformBitArray[i] |= entropy1Bit;
            if (entropy7Bits < threshold) {
                biasBitArray[i] = 1;
            }
        }

        return {uniformBitArray, biasBitArray};
    }

    // Decompose an integer into a four-octet array
    // Input:
    //   value: an integer
    // Return:
    //   Uint8Array: A 4-element array representing an integer in big-endian
    serializeUInt8(value) {
        let bytes = new Uint8Array(4);
        bytes[3] = value & 0xFF;
        bytes[2] = (value >> 8) & 0xFF;
        bytes[1] = (value >> 16) & 0xFF;
        bytes[0] = (value >> 24) & 0xFF;
        return bytes;
    }

    //   msg = int_to_32BE(cohort) + word
    //   digest = SHA1(msg)
    //   [ord(digest[i]) % num_bloombits for i in xrange(num_hashes)]

    // Encode value in a bloom filter
    // Input:
    //   value: A string
    // Return:
    //   Uint8Array: A numHashes-element array representing a bloomfilter
    async getBitEncodingForBloomFilter(value) {
        const blockSize = 20;
        const encoder = new TextEncoder();

        //let encodedValue = encoder.encode(value);
        let encodedCohort = this.serializeUInt8(self.cohort);

        //const totalTaggedLen = encodedValue.length + encodedCohort.length;
        //let encodedTaggedValue = new Uint8Array(totalTaggedLen);
        const totalTaggedLen = value.length + encodedCohort.length;
        let encodedTaggedValue = new Uint8Array(totalTaggedLen);
        encodedTaggedValue.set(encodedCohort);
        encodedTaggedValue.set(value, encodedCohort.length);
        let digestedTaggedValue = await crypto.subtle.digest('SHA-1', encodedTaggedValue);
        let truncatedDigest = new Uint8Array(digestedTaggedValue, 0, self.numHashes);

        truncatedDigest.forEach((elm, idx, arr) => {
            arr[idx] = elm % self.numBloomBits;
        });

        return truncatedDigest;
    }

    // Obtain a random value, with a bias
    // Input:
    //   arr: a (typed) array where the random value is returned
    //   prob: the probability of any element being 1
    // Return:
    //   arr: a (typed) filled with a random value
    //
    // Note: When testing, the returned value is deterministic
    getRandomValuesWithProb(arr, prob) {
        if (self.testing) {
            // Hard-coding for test cases
            let cycle = [0.0, 0.6, 0.0];
            let counter = 0;
            for (let i = 0; i < self.numBloomBits; i++) {
                let val = cycle[counter];
                counter = ++counter % cycle.length;
                if (val < prob) {
                    arr[i] = 1;
                }
            }
            return arr;
        } else {
            crypto.getRandomValues(arr);
            arr.forEach((elm, idx, arr) => {
                if (elm < prob) {
                    arr[idx] = 1;
                }
            });
            return arr;
        }
    }

    // Helper function for getRandomValuesWithProb
    getRandomValuesP(arr) {
        return this.getRandomValuesWithProb(arr, self.probP);
    }

    // Helper function for getRandomValuesWithProb
    getRandomValuesQ(arr) {
        return this.getRandomValuesWithProb(arr, self.probQ);
    }

    // Calculate the Permanent and Instantaneous Responses
    // Input:
    //   valueBloomFilter: A four-octet big-endian bloom filter
    // Return:
    //   Object: arrayPermResp is a four-octet big-endian bloom filter
    //             representing the permanent response
    //           arrayInstResp is a four-octet big-endian bloom filter
    //             representing the instantaneous response
    async getPermAndInstResp(valueBloomFilter) {
        let obj = await this.getPermRandResp(valueBloomFilter);
        let uniform = obj.uniformBitArray, bias = obj.biasBitArray;

        // uniform and bias are LE bit arrays, convert them into BE
        // octet arrays.
        let packedUniform = this.packIntegerArrAsBE(uniform);
        let packedBias = this.packIntegerArrAsBE(bias);

        // Initialize with starting values (the arrays are manipulated
        // in place below).
        let arrayProbOfOne = new Uint8Array(packedUniform);
        let arrayProbOfBi = new Uint8Array(valueBloomFilter);

        // Copied from Google's example python client:
        //
        // Suppose bit i of the Bloom filter is B_i.  Then bit i of the PRR is
        // defined as:
        //
        // 1   with prob f/2
        // 0   with prob f/2
        // B_i with prob 1-f
        // 
        // Uniform bits are 1 with probability 1/2, and f_mask bits are 1 with
        // probability f.  So in the expression below:
        //
        // - Bits in (uniform & f_mask) are 1 with probability f/2.
        // - (bloom_bits & ~f_mask) clears a bloom filter bit with probability
        // f, so we get B_i with probability 1-f.
        // - The remaining bits are 0, with remaining probability f/2.

        // Calculate (bits & ~f_mask)
        arrayProbOfBi.forEach((elm, idx, arr) => {
            arr[idx] = elm & ~(packedBias[idx]);
        });

        // Calculate (uniform & f_mask)
        arrayProbOfOne.forEach((elm, idx, arr) => {
            arr[idx] = elm & packedBias[idx];
        });

        // Calculate (bits & ~f_mask) | (uniform & f_mask)
        let arrayPermResp = new Uint8Array(valueBloomFilter.length);
        arrayPermResp.forEach((elm, idx, arr) => {
            arr[idx] = arrayProbOfBi[idx] | arrayProbOfOne[idx];
        });

        // Get some "randomness"
        let probPEntropy = new Uint8Array(self.numBloomBits);
        let packedRandomValuesP = this.packIntegerArrAsBE(this.getRandomValuesP(probPEntropy));

        let probQEntropy = new Uint8Array(self.numBloomBits);
        let packedRandomValuesQ = this.packIntegerArrAsBE(this.getRandomValuesQ(probQEntropy));

        // Calculate (p_bits & ~prr)
        packedRandomValuesP.forEach((elm, idx, arr) => {
            arr[idx] = elm & ~(arrayPermResp[idx]);
        });

        // Calculate (q_bits & prr)
        packedRandomValuesQ.forEach((elm, idx, arr) => {
            arr[idx] = elm & arrayPermResp[idx];
        });

        // Calculate (p_bits & ~prr) | (q_bits & prr)
        let arrayInstResp = new Uint8Array(valueBloomFilter.length);
        arrayInstResp.forEach((elm, idx, arr) => {
            arr[idx] = packedRandomValuesP[idx] | packedRandomValuesQ[idx];
        });

        return {arrayPermResp, arrayInstResp};
    }

    //   bits = get_bloom_bits()
    //   bloom = 0
    //   for bit_to_set in bloom_bits:
    //     bloom |= (1 << bit_to_set)

    // Convert a value into a bit array
    // Input:
    //   value: A string
    // Return:
    //   Uint8Array: A numBloomBits-element array representing a bloomfilter
    async getBloomFilterBits(value) {
        let bits = await this.getBitEncodingForBloomFilter(value);

        let bloomFilter = new Uint8Array(self.numBloomBits);
        bits.forEach((elm, idx, arr) => {
            bloomFilter[elm] = 1;
        });

        return bloomFilter;
    }

    // Convert an array into an interger
    packInteger(arr, size) {
        let val = 0;
        arr.forEach((elm, idx, arr) => {
            val |= (elm << (idx*size));
        });
        return val;
    }

    // Helper function for packInteger where the
    // elements of the array are bits
    packIntegerBitArr(arr) {
        return this.packInteger(arr, 1);
    }

    // Helper function for packInteger where the
    // elements of the array are octets
    packIntegerOctetArr(arr) {
        return this.packInteger(arr, 8);
    }

    // Convert a bit-array into an octet-array
    packIntegerArr(arr) {
        const bitsPerOctet = 8;
        const lengthNeeded = Math.ceil(arr.length / bitsPerOctet);
        let newArr = new Uint8Array(lengthNeeded);
        arr.forEach((elm, idx, arr) => {
            let shift = idx % 8;
            let newArrIdx = Math.floor(idx / 8);
            newArr[newArrIdx] |= (elm << shift);
        });
        return newArr;
    }

    // Convert the array from big-endian to little-endian
    // Input:
    //   arr: A Uint8Array where the integer value is encoded in big-endian
    // Return:
    //   U8IntArray: An array where the integer value is encoded in little-endian
    integerArrBE2LE(arr) {
        return new Uint8Array(arr).reverse();
    }

    // Convert the array from little-endian to big-endian
    // Input:
    //   arr: A Uint8Array where the integer value is encoded in little-endian
    // Return:
    //   U8IntArray: An array where the integer value is encoded in big-endian
    integerArrLE2BE(arr) {
        return new Uint8Array(arr).reverse();
    }

    // Add padding octets, up to len octets, if needed.
    // Input:
    //   len: An integer defining the maximum amount of padding octets
    //   arr: A Uint8Array that should be padded, if it is less than len.
    // Return:
    //   Uint8Array: The resulting padded array
    padIntegerArr(len, arr) {
        if (arr.length >= len) {
            return arr;
        }
        let paddedArr = new Uint8Array(len);
        paddedArr.set(arr);
        return paddedArr;
    }

    // Convert little-endian bit array into big-endian octet array
    // Input:
    //   arr: A Uint8Array little-endian bit array
    // Return:
    //   Uint8Array: An octet array where the integer value of arr is encoded in big-endian 
    packIntegerArrAsBE(arr) {
        let packedArr = this.packIntegerArr(arr);
        packedArr = this.padIntegerArr(4, packedArr);
        return this.integerArrLE2BE(packedArr);
    }

    // Encode a value as an instantaneous response bloomfilter
    // Input:
    //   value: A string
    // Return:
    //   Uint8Array: A four-octet big-endian bloom filter
    //               representing the instantaneous response
    async getEncoding(value) {
        const encoder = new TextEncoder();
        let encodedMessage = encoder.encode(value);
        let bloomFilter = await this.getBloomFilterBits(encodedMessage);
        let packedBF = this.packIntegerArrAsBE(bloomFilter);
        let obj = await this.getPermAndInstResp(packedBF);
        return obj.arrayInstResp;
    }
}
