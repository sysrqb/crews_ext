async function run() {
    {
        let elm = document.getElementById("hmacTestVal");
        let rappor = new RapporEncoder({
            cohort: 0,
            numHashes: 2,
            numBloomBits: 8,
            probF: 0.30, 
            probP: 0.40,
            probQ: 0.70, 
            secret: "secret",
            testing: true
        });

        let words = ['foo', 'v1', 'v2', 'v3', 'abc'];
        let expected = {
            'foo': [
                0x77,
                0x3b,
                0xa4,
                0x46,
                0x93,
                0xc7,
                0x55,
                0x3d,
                0x6e,
                0xe2,
                0x0f,
                0x61,
                0xea,
                0x5d,
                0x27,
                0x57,
                0xa9,
                0xa4,
                0xf4,
                0xa4,
                0x4d,
                0x28,
                0x41,
                0xae,
                0x4e,
                0x95,
                0xb5,
                0x2e,
                0x4c,
                0xd6,
                0x2d,
                0xb4
            ],
            'v1': [
                0xb7,
                0x74,
                0xec,
                0x50,
                0x77,
                0x7c,
                0xf6,
                0xa0,
                0x54,
                0x21,
                0x84,
                0xbf,
                0x8f,
                0x3c,
                0x53,
                0x9d,
                0x1e,
                0x93,
                0x45,
                0x0c,
                0x8f,
                0xd1,
                0xb8,
                0x83,
                0xdc,
                0x78,
                0x8b,
                0xea,
                0xbd,
                0xfb,
                0xa0,
                0x4b
            ],
            'v2': [
                0x05,
                0x19,
                0x42,
                0x60,
                0x76,
                0x8e,
                0x8c,
                0x08,
                0x71,
                0xe6,
                0x1d,
                0x87,
                0xfc,
                0xd6,
                0x9c,
                0x3b,
                0x0e,
                0x9b,
                0x19,
                0x2c,
                0x1d,
                0xdc,
                0xcd,
                0x71,
                0x74,
                0xdc,
                0xf2,
                0x35,
                0xc5,
                0x47,
                0x52,
                0x43
            ],
            'v3': [
                0x8c,
                0x1d,
                0xcb,
                0x1a,
                0xe5,
                0xa6,
                0x44,
                0x05,
                0x6d,
                0xd5,
                0xe2,
                0x80,
                0xff,
                0xfe,
                0xb3,
                0x70,
                0xed,
                0x03,
                0xd4,
                0x85,
                0xb2,
                0xe5,
                0xae,
                0x15,
                0x70,
                0x97,
                0xce,
                0x61,
                0x8b,
                0xe8,
                0xac,
                0xea
            ],
            "abc": [
                0x99,
                0x46,
                0xda,
                0xd4,
                0xe0,
                0x0e,
                0x91,
                0x3f,
                0xc8,
                0xbe,
                0x8e,
                0x5d,
                0x3f,
                0x7e,
                0x11,
                0x0a,
                0x4a,
                0x9e,
                0x83,
                0x2f,
                0x83,
                0xfb,
                0x09,
                0xc3,
                0x45,
                0x28,
                0x5d,
                0x78,
                0x63,
                0x8d,
                0x8a,
                0x0e
            ]
        };
        const encoder = new TextEncoder();
        let encodedSecret = encoder.encode("secret");
        let results = []
        for (word of words) {
            let encodedMessage = encoder.encode(word);
            let mac = await rappor.hmac(encodedSecret, encodedMessage);
            let hashedValueArr = new Uint8Array(mac);
            results.push(hashedValueArr);
        }
        if (elm !== undefined && elm !== null) {
            pass = true;
            let i = 0;
            for (word of words) {
                let expectedValue = expected[word];
                let resultValue = results[i++];
                expectedValue.forEach((elm, idx, arr) => {
                    pass = (pass && elm == resultValue[idx]);
                });
            }
            elm.innerHTML += (pass? "pass" : "fail");
        }
            
    }

    {
        let elm = document.getElementById("packIntTestVal");
        let rappor = new RapporEncoder({
            cohort: 0,
            numHashes: 2,
            numBloomBits: 8,
            probF: 0.30, 
            probP: 0.40,
            probQ: 0.70, 
            secret: "secret",
            testing: true
        });

        let arr = [1, 0, 1, 1, 0, 1];
        let expected = 45;
        let packed = rappor.packIntegerBitArr(arr);
        if (elm !== undefined && elm !== null) {
            elm.innerHTML += ((expected == packed)? "pass" : "fail");
        }

    }

    {
        let elm = document.getElementById("packIntArrTestVal");
        let rappor = new RapporEncoder({
            cohort: 0,
            numHashes: 2,
            numBloomBits: 8,
            probF: 0.30, 
            probP: 0.40,
            probQ: 0.70, 
            secret: "secret",
            testing: true
        });

        let arr = [1, 0, 1, 1, 0, 1, 0, 0, 1];
        let expected = new Uint8Array([301]);
        let packed = rappor.packIntegerArr(arr);
        if (elm !== undefined && elm !== null) {
            pass = true;
            let i = 0;
            expected.forEach((elm, idx, arr) => {
                pass = (pass && (elm == packed[idx]));
            });
            elm.innerHTML += (pass? "pass" : "fail");
        }

    }

    {
        let elm = document.getElementById("hmacBloomTestVal");
        let rappor = new RapporEncoder({
            cohort: 1,
            numHashes: 2,
            numBloomBits: 8,
            probF: 0.30, 
            probP: 0.40,
            probQ: 0.70, 
            secret: "secret",
            testing: true
        });

        let words = ['foo', 'v1', 'v2', 'v3', 'abc'];
        let expected = {
            'foo': [
                0x49,
                0xe5,
                0x07,
                0x5d,
                0xd9,
                0x80,
                0x92,
                0x1b,
                0x41,
                0x90,
                0xda,
                0xd9,
                0x05,
                0x6c,
                0x37,
                0x72,
                0x6e,
                0xc0,
                0xf6,
                0x57,
                0x46,
                0x2e,
                0x08,
                0x45,
                0xf3,
                0x50,
                0x9a,
                0xc7,
                0x43,
                0x4e,
                0x29,
                0xf3
            ],
            'v1': [
                0xb7,
                0xf2,
                0x73,
                0x01,
                0xc7,
                0x79,
                0x59,
                0x5d,
                0x58,
                0xb5,
                0x9c,
                0xaf,
                0x47,
                0x77,
                0x03,
                0xde,
                0x63,
                0x8f,
                0x10,
                0xe9,
                0xcf,
                0xe3,
                0xd7,
                0x23,
                0xd5,
                0x89,
                0xcf,
                0xc3,
                0x4c,
                0x52,
                0x46,
                0x3b
            ],
            'v2': [
                0x45,
                0xdb,
                0x57,
                0x30,
                0xf0,
                0x65,
                0x00,
                0x77,
                0x81,
                0x8d,
                0x22,
                0x3e,
                0x3f,
                0x2a,
                0xa1,
                0x64,
                0xdc,
                0x68,
                0x20,
                0x3a,
                0x0b,
                0xfb,
                0x08,
                0x83,
                0x3a,
                0xe4,
                0x1c,
                0x76,
                0x50,
                0x24,
                0x13,
                0xa8
            ],
            'v3': [
                0xe2,
                0x5a,
                0x27,
                0xa6,
                0xc4,
                0x23,
                0xdc,
                0x7a,
                0x57,
                0x67,
                0xed,
                0xcc,
                0x77,
                0x36,
                0x87,
                0x16,
                0x36,
                0x29,
                0x84,
                0x6d,
                0x21,
                0xd2,
                0x9f,
                0x1c,
                0x87,
                0x34,
                0x81,
                0xf4,
                0x07,
                0x61,
                0x17,
                0x61
            ],
            "abc": [
                0x19,
                0x85,
                0x30,
                0x33,
                0x92,
                0x3c,
                0x24,
                0x21,
                0x3a,
                0x81,
                0x89,
                0x44,
                0xdf,
                0x3a,
                0x8f,
                0x33,
                0xcb,
                0x06,
                0x94,
                0x22,
                0x5a,
                0x49,
                0x90,
                0xd4,
                0xdb,
                0x5a,
                0x75,
                0x13,
                0xcb,
                0x8e,
                0xb9,
                0x85
            ]
        };
        const encoder = new TextEncoder();
        let encodedSecret = encoder.encode("secret");
        let results = []
        for (word of words) {
            let encodedMessage = encoder.encode(word);
            let bits = await rappor.getBloomFilterBits(encodedMessage);
            let packed = rappor.packIntegerArrAsBE(bits);
            let mac = await rappor.hmac(encodedSecret, packed);
            let hashedValueArr = new Uint8Array(mac);
            results.push(hashedValueArr);
        }
        if (elm !== undefined && elm !== null) {
            pass = true;
            let i = 0;
            for (word of words) {
                let expectedValue = expected[word];
                let resultValue = results[i++];
                expectedValue.forEach((elm, idx, arr) => {
                    pass = (pass && elm == resultValue[idx]);
                });
            }
            elm.innerHTML += (pass? "pass" : "fail");
        }
    }

    {
        let elm = document.getElementById("bitEncodingBloomFilterTestVal");

        let expected = [
            [12, 3],
            [5, 6],
            [5, 2],
            [13, 10],
            [12, 7],
            [3, 14],
            [13, 7],
            [10, 7],
            [15, 2],
            [11, 14],
            [3, 15],
            [4, 10],
            [3, 4],
            [5, 1],
            [5, 0],
            [6, 7],
            [11, 15],
            [5, 4],
            [11, 11],
            [6, 2],
            [14, 1],
            [8, 6],
            [9, 11],
            [5, 10],
            [14, 7],
            [14, 3],
            [4, 10],
            [3, 10],
            [4, 4],
            [2, 0],
            [13, 3],
            [1, 14],
            [8, 6],
            [9, 11],
            [3, 12],
            [15, 3],
            [8, 6],
            [8, 13],
            [8, 5],
            [15, 6],
            [12, 9],
            [4, 13],
            [13, 6],
            [11, 6],
            [11, 7],
            [0, 12],
            [8, 3],
            [1, 3],
            [0, 0],
            [15, 12],
            [14, 0],
            [0, 0],
            [13, 9],
            [1, 14],
            [15, 4],
            [0, 9],
            [7, 3],
            [13, 7],
            [7, 10],
            [7, 6],
            [1, 7],
            [5, 11],
            [3, 8],
            [12, 0],
            [5, 6]
        ];

        let result = [];
        const encoder = new TextEncoder();
        let encodedMessage = encoder.encode('foo');
        for (let i = 0; i < 64; i++) {
            let rappor = new RapporEncoder({
                cohort: i,
                numHashes: 2,
                numBloomBits: 16,
                probF: 0.30, 
                probP: 0.40,
                probQ: 0.70, 
                testing: true
            });

            let bits = await rappor.getBitEncodingForBloomFilter(encodedMessage);
            result.push(bits);
        }
        {
            let rappor = new RapporEncoder({
                cohort: 1,
                numHashes: 2,
                numBloomBits: 8,
                probF: 0.30, 
                probP: 0.40,
                probQ: 0.70, 
                testing: true
            });
            let bits = await rappor.getBitEncodingForBloomFilter(encodedMessage);
            result.push(bits);
        }
        let pass = true;
        for (let i = 0; i < expected.length; i++) {
            let [expectedUniformMask, expectedBiasMask] = expected[i];
            let [resultUniformMask, resultBiasMask] = result[i];
            pass = pass && (expectedUniformMask === resultUniformMask && expectedBiasMask === resultBiasMask);
        }
        if (elm !== undefined && elm !== null) {
            elm.innerHTML = (pass ? "pass" : "fail");
        }
    }

    {
        let elm = document.getElementById("prrTestVal");
        let rappor = new RapporEncoder({
            cohort: 0,
            numHashes: 2,
            numBloomBits: 8,
            probF: 0.50, 
            probP: 0.40,
            probQ: 0.70, 
            secret: "secret",
            testing: true
        });

        let expected = [
            [17, 58],
            [3, 159],
            [150, 202],
            [243, 203]
        ];

        let results = [];
        let words = ['v1', 'v2', 'v3', 'foo'];
        for (word of words) {
            const encoder = new TextEncoder();
            let encodedMessage = encoder.encode(word);
            let masks = await rappor.getPermRandResp(encodedMessage);
            let uniformMask = rappor.packIntegerBitArr(masks.uniformBitArray);
            let biasMask = rappor.packIntegerBitArr(masks.biasBitArray);

            results.push([uniformMask, biasMask]);
        }
        let pass = true;
        let failReason = [];
        for (let i = 0; i < expected.length; i++) {
            let [resultUniformMask, resultBiasMask] = results[i];
            let [expectedUniformMask, expectedBiasMask] = expected[i];
            let success = (expectedUniformMask === resultUniformMask && expectedBiasMask === resultBiasMask)
            pass = pass && success;
            if (!success) {
                let msg = `Index ${i}, Expected: (${expectedUniformMask}, ${expectedBiasMask}), Got: (${resultUniformMask}, ${resultBiasMask}))`;
                failReason.push(msg);
            }
        }
        if (elm !== undefined && elm !== null) {
            elm.innerHTML = (pass ? "pass" : "fail: ") + (pass ? "" : failReason);
        }
    }

    {
        let rappor = new RapporEncoder({
            probF: 0.50, 
            probP: 0.50,
            probQ: 0.75, 
            numBloomBits: 16,
            numCohorts: 64,
            cohort: 0,
            secret: "secret",
            testing: true
        });

        let encoding = await rappor.getEncoding("abc");
        let packed = rappor.packIntegerOctetArr(rappor.integerArrBE2LE(encoding));
        
        let elm = document.getElementById("encoderTestVal");
        if (elm !== undefined && elm !== null) {
            let expected = 56301;
            elm.innerHTML = ((packed == expected) ? "pass" : "fail") + " (Got: " + packed + ", Expected: " + expected + ")";
        }
    }
}

run();
